"""
Module for lensing your face. 
Contains software to simulate halos and compute lensing
deflection angles
"""

import numpy as np
import matplotlib.pyplot as plt

"""
The global variables below set the cosmological parameters and some 
relevant unit conversions and other constants
"""
global G, om, ol, h, m_per_km, m_per_mpc, kg_per_msun, c
om = 0.27
ol = 0.73
h = 0.72
G = 6.67e-11 # m^3/(kg s^2)
m_per_km = 1000
m_per_mpc = 3.086e22
kg_per_msun = 2e30
c = 3.0e5 # km/s

"""
lensmap produces a lensing convergence field with multiple lenses, all set to 
the same redshift, at random positions
"""

def gen_random_lenses(nlenses, npix = 128, pixscale = 1, posx = 64, posy = 64,\
                          zl = 0.2, zs = 1.0, random_positions=True, \
                          random_masses = True, sig_m = 0.1, sig_x = 8, \
                          mass = 1.e13):
    
    if(random_positions):
        assert sig_x !=0
        posx = np.random.normal(posx, sig_x, nlenses)
        posy = np.random.normal(posy, sig_x, nlenses)
    else:
        posx = np.repeat(posx,nlenses)
        posy = np.repeat(posy,nlenses)
        
    if(random_masses):
        # Generates a set of log-normally distributed masses with mean 
        # mean_m_log and standard deviation sig_m
        assert sig_m !=0
        mean_m_log=np.log10(mass)
        logm = np.random.normal(mean_m_log, sig_m, nlenses) 
        mass = pow(10,logm)
    else:
        mass = np.repeat(mass, nlenses)
        
    s=(npix,npix)
    kappa = np.zeros(s)
    for i in range(nlenses):
        kappa += gen_kappa(npix=npix, pixscale = pixscale, \
                               posx = posx[i], \
                               posy = posy[i], mass = mass[i], \
                               zl = zl, \
                               zs = zs)
    
    alpha1, alpha2=compute_alpha(kappa)
    
    return kappa, alpha1, alpha2 
    #plot_kappa(kappa, pixscale = pixscale, npix=npix)


"""
kappa - produces an image of npix x npix containing the convergence
for a singular isothermal sphere lens of mass (M200c) and redshift zl
as experienced by a background source at redshift zs
The pixel scale should be in arcseconds and the position of the lens centre 
(posx, posy) specified in pixel units
"""

def gen_kappa(npix=128, pixscale=1, posx = 64, posy = 64, mass = 1e14, \
              zl = 0.2, zs = 1.0, verbose=False):

    if(verbose):
        vb = True
    else:
        vb = False

    """
    First compute lens properties:

    R200 - the radius within which the mean density is 
    200x the critical density at redshift zl, given in Mpc
    sigma_v - the velocity dispersion for an isothermal sphere
    with R200 given by the above computation, given in km/s
    theta_E - the Einstein radius of the lens, assuming a 
    source at redshift zs, given in arcseconds
    """

    r200 = get_r200(mass, zl, verbose=vb) # Mpc
    sv = get_sigv(r200, zl, verbose=vb) # km/s
    theta_E = get_einstein_radius(sv, zl, zs, verbose=vb)
    
    """
    Now generate 2D arrays of positions x and y relative to the lens centre
    in arcseconds, and compute the scalar radius of each grid point to the 
    lens centre
    """
        
    x=np.resize(np.arange(npix), [npix, npix]) #This actually repeats the first row npix times. 

        
    y = (x.T + 0.5 - posy)*pixscale
    x = (x+0.5 - posx)*pixscale
    r = np.sqrt(pow(x,2)+pow(y,2))

    kappa = theta_E/(2.*r)
    
    
   
    return kappa

# get_r200 computes the characteristic radius of a halo i.e. the radius within which 
# the mean density is 200x the critical density
def get_r200(mass, zl, verbose = False):

    rhocrit = get_rhocrit(zl) # msun/Mpc^3
    
    r200 = pow(3*mass/(800*np.pi*rhocrit),(1./3.)) # Mpc
    if(verbose):
        print("R200 = {0:.3f} Mpc".format(r200))
    return r200

# get_rhocrit computes the critical density of the Universe at the specified redshift
def get_rhocrit(zl):
    
    H0 = 100*h # km/s/Mpc
    rhocrit = 3*pow(H0,2)*(om*pow((1+zl),3)+ol)/(8*np.pi*G) * \
        pow(m_per_km,2)/kg_per_msun*m_per_mpc # msun/Mpc^3
    return rhocrit

# get_sigv comptues the velocity dispersion of the isothermal halo given a characteristic
# radius R200
def get_sigv(r200,zl,verbose=False):
    
    rhocrit = get_rhocrit(zl)
    sigv2 = 400*np.pi*G*rhocrit*pow(r200,2)/3.*kg_per_msun\
        /m_per_mpc # (m/s)^2 
    if (verbose):
        print "The velocity dispersion is {0:.3f} km/s"\
            .format(np.sqrt(sigv2)/m_per_km)
    return np.sqrt(sigv2)/m_per_km

# get_einstein_radius computes the einstein radius of the lens given a velocity dispersion
# and the source and lens redshifts
def get_einstein_radius(sigv, zl, zs, verbose = False):

    """
    Computes the einstein radius using cosmocalc for the distance calculation
    """

    import cosmocalc as co
    cosmolens = co.cosmocalc(zl, H0=h*100, WM=om, WV=ol)
    dl = cosmolens['DA_Mpc']
    cosmosource = co.cosmocalc(zs, H0=h*100, WM=om, WV=ol)
    ds = cosmosource['DA_Mpc']
    dls = ds - (1+zl)/(1+zs)*dl
    if verbose:
        print("DL = {0:} Mpc, DS = {1:} Mpc, DLS = {2:} Mpc".\
                  format(dl, ds, dls))
        
    thetaE = 2*np.pi*pow(sigv,2)/pow(c,2)*dls/ds
    if(verbose):
        print ("The einstein radius is {0:.3f} arcseconds"\
                   .format(thetaE*206265))
    return thetaE*206265. # thetaE returns in arcseconds

# plot_kappa does what it says on the tin!    
def plot_kappa(kappa, pixscale = 1.):
    from matplotlib.colors import LogNorm
    
    n = np.shape(kappa)
    npix = n[0]

    plt.subplot(1,1,1)

    y, x = np.mgrid[slice(0,npix*pixscale+pixscale,pixscale),\
                        slice(0,npix*pixscale+pixscale,pixscale)]

    cmap = plt.get_cmap('hot')
    nonzero = np.where(kappa!=0)
    im = plt.pcolormesh(x,y,kappa, norm = LogNorm(vmin=kappa[nonzero].min(), \
                                                      vmax = kappa.max()),\
                            cmap = cmap)
    
    plt.colorbar()
    plt.title('Convergence map')
    plt.axis([x.min(), x.max(),y.min(),y.max()])
    plt.show()
    #plt.draw()

"""
Computes the deflection angles from a kappa map
"""
def compute_alpha(kappa, pad = 4,pixscale=1):
    sz = np.shape(kappa)
    # Apply padding to the convergence map to minimise edge effects in the FFT
    padkappa = add_padding(kappa, pad = pad)
    szp = np.shape(padkappa)

    # now compute the wave numbers at each pixel
    thetax = (np.shape(kappa))[0]*pixscale/206265.
    thetay = (np.shape(kappa))[1]*pixscale/206265.

    
    ftkap = np.fft.fft2(padkappa)
    
    #print(np.shape(ftkap))
    [l1,l2] = np.meshgrid(2*np.pi*(np.arange(szp[0])-(szp[0])/2)/thetax,\
                              2*np.pi*(np.arange(szp[1])-(szp[1])/2)/thetay)
    
    
    l1 = np.fft.fftshift(l1)
    l2 = np.fft.fftshift(l2)
    
        
    lsq = l1**2+l2**2
    psi = ftkap*0
    psi[np.nonzero(lsq)] = -2/lsq[np.nonzero(lsq)]*ftkap[np.nonzero(lsq)]

    #psi = -2/(l1**2+l2**2)*ftkap
    #centre = np.where((l1==0) & (l2==0))
    #psi[centre] = 0

    ftalpha1 = psi*1j*l1
    ftalpha2 = psi*1j*l2

    
    alpha1 = np.fft.ifft2(ftalpha1)
    alpha2 = np.fft.ifft2(ftalpha2)
    
    
    ns1 = sz[0]*(pad-1)/2
    ns2 = sz[1]*(pad-1)/2

    #Just changed alpha to a tuple of two arrays
    alpha = (alpha1.real)[ns1:ns1+sz[0],ns2:ns2+sz[1]], (alpha2.real)[ns1:ns1+sz[0],ns2:ns2+sz[1]] 
    
    return alpha

def test_alpha(pad = 4):
    
    kappa = gen_kappa()
    #plot_kappa(kappa)

    alpha = compute_alpha(kappa,pad = pad)*206265.
    plot_kappa(np.absolute(alpha))
    return alpha
        

def add_padding(inp, pad=4):
    
    oldsize = np.shape(inp)
    newsize = (pad*oldsize[0], pad*oldsize[1])
    
    indx = (pad-1)*oldsize[0]/2
    indy = (pad-1)*oldsize[1]/2

    padinp = np.zeros(newsize)
    padinp[indx:indx+oldsize[0],indy:indy+oldsize[1]] = inp
    return padinp
