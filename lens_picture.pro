pro lens_picture, filename, outname=outname

read_jpeg, filename, img_in
if not keyword_set(outname) then outname = 'test_lensed.jpg'

z_l = 0.2
z_s = 1.0
pix_scale_bg = 0.3
pix_scale_fg = 0.1
pix_scale_bg=pix_scale_bg/206265.
c=3.e5;m/s
km_Mpc=3.086e19;km/Mpc
H_0=71.;/km_Mpc
pad = 4

D_s=c*z_s/(H_0*(1+z_s));Mpc
D_l=c*z_l/(H_0*(1+z_l));Mpc
D_ls=D_s-(1+z_s)/(1+z_l)*D_l;Mpc

inf = 'kappa_rs5_alpha.fits.gz'
;rdfits_struct,'kappa_rs5_alpha.fits.gz',lens
lensmap = {alpha1:readfits(inf,exten_no=1), alpha2:readfits(inf,exten_no=2), x:readfits(inf,exten_no=3), y:readfits(inf,exten_no=4),map:readfits(inf,exten_no=0)}

nfg = (size(lensmap.alpha1))[1]
img_out = dblarr(3,nfg,nfg)

for icolour = 0, 2 do begin

   dum = reform(img_in[icolour,*,*])

   N = max((size(dum))[2:3])
   nx = (size(dum))[2]
   ny = (size(dum))[3]

   x = dblarr(nx,ny)
   y = x
   for i = 0, nx-1 do for j = 0, ny-1 do begin
      x[i,j] = double(i-float(nx)/2.)*pix_scale_bg
      y[i,j] = double(j-float(ny)/2.)*pix_scale_bg
   endfor

   img_map = {map:dum, x:x, y:y}

   ray_trace, lensmap, img_map, pix_scale_bg, lensed;,rescale=2
   img_out[icolour,*,*] = lensed

   ;plt_image,lensed,/colbar,/frame
   ;stop
endfor

tv,img_out, true=1

write_jpeg,outname,img_out,true=1

return
end
   
   
