from __future__ import division
import pylab as pl
import pyfits, sys, ray_tracing, time

#Tiny class for the lensmap (could also use a dictionary)
class LensMap:
    alpha1=0
    alpha2=0
    x=0
    y=0
    map=0
    
    #Initialise the class from a fitsfile
    #Can overload this constructor to call another python module instead
    def __init__(self, fitsfile):
        h=pyfits.open(fitsfile)
        self.map=h[0].data
        self.alpha1=h[1].data
        self.alpha2=h[2].data
        self.x=h[3].data
        self.y=h[4].data
        print self.x
        h.close()
        
#Output image map class
class ImgMap:
    map=0
    x=0
    y=0
    
    def __init__(self, map, x, y):
        self.map=map
        self.x=x
        self.y=y

T1=time.time()

#Get input and output files (robust to human error)
if len(sys.argv)<2:
    print 'ERROR: Must input a file'
    sys.exit(0)
else:
    infilename=sys.argv[1]
    if len(sys.argv)<3:
        outfilename='test_lensed.jpg'
    else:
        outfilename=sys.argv[2]
        

img_in=pl.imread(infilename)

pix_scale_bg = 0.3
pix_scale_bg=pix_scale_bg/206265.

#create a lensmap object from a fits file

lm=LensMap('kappa_rs5_alpha.fits.gz')

npix_fg=pl.shape(lm.alpha1)[0]

#Output image in 3 colours
#Note: I've reversed the axis order from the IDL version because of the way python reads in images
img_out=pl.zeros([npix_fg, npix_fg, 3])

for icolour in range(3):
    im=img_in[:, :, icolour]
    sh=pl.shape(im)
    nx=sh[0]
    ny=sh[1]
    
    #Repeat the x and y's into 2d arrays
    x=(pl.resize(pl.arange(nx), (ny, nx))-nx/2)*pix_scale_bg
    y=(pl.resize(pl.arange(ny), (nx, ny)).T-nx/2)*pix_scale_bg
    
    img_map=ImgMap(im, x, y)
    
    lensed_img=ray_tracing.ray_trace(lm, img_map, pix_scale_bg)
    
    img_out[:, :, icolour]=lensed_img
    

#Save the image
pl.imsave(outfilename, img_out)

print 'time', time.time()-T1
    
    
    




























