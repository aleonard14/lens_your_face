from __future__ import division
import pylab as pl
from PIL import Image
import pyfits
from scipy.ndimage.interpolation import zoom, map_coordinates

#Tiny class for the lensmap (could also use a dictionary)
class LensMap:
    alpha1=0
    alpha2=0
    x=0
    y=0
    map=0
    
    #Initialise the class from a list of kappa,alphas and x,y
    def __init__(self, kappa, a1, a2):
        self.map=kappa
        self.alpha1=a1
        self.alpha2=a2
        
        sh=pl.shape(self.map)
        nx=sh[0]
        ny=sh[1]
        pix_scale_bg=0.3/206265.
        
        #This gets reset anyway in do_lensing
        self.x=(pl.resize(pl.arange(nx), (ny, nx))-nx/2)*pix_scale_bg
        self.y=(pl.resize(pl.arange(ny), (nx, ny)).T-nx/2)*pix_scale_bg
    
    #Overloaded constructor. To use create an object using lm=LensMap.fromfits(fitsfile)
    @classmethod
    def fromfits(cls, fitsfile):
        h=pyfits.open(fitsfile)
        map=h[0].data
        alpha1=h[1].data
        alpha2=h[2].data
        x=h[3].data
        y=h[4].data
        h.close()
        return cls(map, alpha1, alpha2)
        


#Given a lensmap and an image, performs the ray tracing
#INPUT: lm - lensing map object
#       img_map - background image object
#       pix_scale_bg - scaling for background pixels
#       rescale - optional keyword argument to rescale image
#OUTPUT: lensed - the lensed image
def ray_trace(lm, img_map, pix_scale_bg,  **kwargs):
    if 'rescale' in kwargs:
        rescale=kwargs['rescale']
    else:
        rescale=1
        
    nx=(int)(pl.shape(lm.map)[0]*rescale)
    nb=pl.shape(img_map)[0]
    
    b1_proj=zoom(lm.x-lm.alpha1, [nx/pl.shape(lm.x)[0], nx/pl.shape(lm.x)[1]])/pix_scale_bg+nb/2
    b2_proj=zoom(lm.y-lm.alpha2, [nx/pl.shape(lm.y)[0], nx/pl.shape(lm.y)[1]])/pix_scale_bg+nb/2

    lensed_map=255-map_coordinates(img_map, [ b2_proj, b1_proj], mode='nearest')

    
    return lensed_map
        

#Do the actual lensing given an input image and deflection map
#INPUT: img_in - input image as a PIL image
#       lens - the kappa,alpha and x,y values of the lens as a LensMap object
        
def do_lensing(img_in, lm):
    img_in=pl.array(img_in)
    pix_scale_bg = 0.3
    pix_scale_bg=pix_scale_bg/206265.

    #create a lensmap object from a fits file

    #lm=LensMap('kappa_rs5_alpha.fits.gz')

    npix_fg=pl.shape(lm.alpha1)[0]

    #Output image in 3 colours
    #Note: I've reversed the axis order from the IDL version because of the way python reads in images
    img_out=pl.zeros([npix_fg, npix_fg, 3])

    for icolour in range(3):
        im=img_in[:, :, icolour]
        sh=pl.shape(lm.map)
        nx=sh[0]
        ny=sh[1]
        
        #Repeat the x and y's into 2d arrays
        x=(pl.resize(pl.arange(nx), (ny, nx))-nx/2)*pix_scale_bg
        y=(pl.resize(pl.arange(ny), (nx, ny)).T-nx/2)*pix_scale_bg
        lm.x=x
        lm.y=y
        
        lensed_img=ray_trace(lm, im, pix_scale_bg)
        
        img_out[:, :, icolour]=lensed_img
    return Image.fromarray(255-img_out.astype('uint8'), 'RGB')
    





























