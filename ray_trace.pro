pro ray_trace, kappa_map, bg_map,pix_scale,lens_map,$
               rescale=rescale

compile_opt idl2

c=3.e8;m/s
km_Mpc=3.086e19;km/Mpc
H_0=71./km_Mpc; inv_seconds
if not keyword_set(rescale) then rescale=1.

;; feed in kappa and bg maps as structures with tags for distance and
;; coordinates in arcseconds. later, can modify this to read in
;; several different bg_maps with different distances and centres.

kappa=kappa_map.map
bg=bg_map.map
theta1=kappa_map.x
theta2=kappa_map.y
alpha1=kappa_map.alpha1
alpha2=kappa_map.alpha2

nx=long(fix(((size(kappa))[1])*rescale))
nb=(size(bg))[1]
lens_map=dblarr(nx,nx)*0.0

;; NOTE: all angles must be in radians

b1_proj=congrid(theta1-alpha1,nx,nx,/interp)/pix_scale+nb/2.
b2_proj=congrid(theta2-alpha2,nx,nx,/interp)/pix_scale+nb/2.

for i=long(0),nx*nx-1 do begin
    ipix=i mod nx
    jpix=i/nx
    
    lens_map[ipix,jpix]=interpolate(bg,b1_proj[ipix,jpix],b2_proj[ipix,jpix],$
                                    cubic=-0.5)
endfor

return
end


