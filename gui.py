from __future__ import division
import Tkinter as tk
from PIL import Image, ImageTk
import cv2, lens_image, lenses, os
from matplotlib.colors import LogNorm
import pylab as pl


class Gui:
    font_settings=("Purisa", 16)
    
    def __init__(self):
        self.cam_num=0 #Needs to be 1 if there's two webcams
        self.cap=cv2.VideoCapture(self.cam_num)
        self.root=tk.Tk()
        #self.z=tk.DoubleVar() #Redshift of cluster
        #self.z.set(0.2)
        #self.M=tk.DoubleVar() #Mass of cluster
        #self.M.set(1e14)
        self.z=0.2
        self.M=1e14
        self.ngal=10
        
        
        #Initialise the lensmap. Change this with the cluster button
        self.lm=lens_image.LensMap.fromfits('kappa_rs5_alpha.fits.gz')
    
        ############### make full screen ###########################
        self.root.geometry("{0}x{1}+0+0".format(self.root.winfo_screenwidth(),self.root.winfo_screenheight()))
        self.wid=self.root.winfo_screenwidth()
        self.hgt=self.root.winfo_screenheight()

        self.imgwid=(int)(0.9*self.wid//3)
        self.imghgt=self.hgt//2
        
        ################ input image ############################
        self.imgFrame1=tk.Frame(self.root)
        im=Image.open('face.jpg')
        im=self.make_square(im)
        self.img=self.rescale_image(im)
        self.inputTkImage=ImageTk.PhotoImage(self.img)
        self.inputImage=tk.Label(master=self.imgFrame1, image=self.inputTkImage, borderwidth=(int)(0.05*self.wid//3))
        self.inputImage.pack()
        
        ############## lens ###################################
        self.imgFrame2=tk.Frame(self.root)
        im2=Image.open('lens.jpg')
        self.img2=self.rescale_image(im2)
        self.lensTkImage=ImageTk.PhotoImage(self.img2)
        self.lensImage=tk.Label(master=self.imgFrame2, image=self.lensTkImage, borderwidth=(int)(0.05*self.wid//3))
        self.lensImage.pack()
        
        ############# output image ###########################
        self.imgFrame3=tk.Frame(self.root)
        im3=Image.open('face.jpg')
        self.img3=self.rescale_image(im3)
        self.outputTkImage=ImageTk.PhotoImage(self.img3)
        self.outputImage=tk.Label(master=self.imgFrame3, image=self.outputTkImage, borderwidth=(int)(0.05*self.wid//3))
        self.outputImage.pack()
        self.lensButton() #Initial lensing
        
        ############ take photo button #####################
        self.button_photo = tk.Button(self.root, text="Take photo",font=self.font_settings,  command=self.photoButton)
        
        ########### change cluster button ############################
        self.button_cluster = tk.Button(self.root, text="Update cluster",font=self.font_settings,  command=self.clusterButton)
        
        ########### do lens button ############################
        self.button_lens = tk.Button(self.root, text="Lens!",font=self.font_settings,  command=self.lensButton)
        
        
        ########## redshift slider #########################
        self.redshiftFrame=tk.Frame(self.root)
        self.redshiftLabel=tk.Label(self.redshiftFrame, text='Redshift = %.1f' %self.z, font=self.font_settings)
        self.redshiftLabel.pack(side='top',anchor='w')
        self.redshift_scale = tk.Scale(self.redshiftFrame, command=self.set_redshift, from_=0.1, to=1.0, resolution=0.1, orient='horizontal', length=0.9*self.wid, width=0.05*self.hgt, showvalue=0)
        self.redshift_scale.set(self.z)
        self.redshift_scale.pack(side='top')
        
        ########### mass slider ###########################
        self.massFrame=tk.Frame(self.root)
        self.massLabel=tk.Label(self.massFrame, text='Galaxy Mass = %.1e' %self.M, font=self.font_settings)
        self.massLabel.pack(side='top',anchor='w')
        self.mass_scale = tk.Scale(self.massFrame, command=self.set_mass, from_=11, to=15, resolution=0.2, orient='horizontal', length=0.9*self.wid, width=0.05*self.hgt, showvalue=0)
        self.mass_scale.set(self.M)
        self.mass_scale.pack(side='top')
        
        ########### number of galaxies slider ###########################
        self.galFrame=tk.Frame(self.root)
        self.galLabel=tk.Label(self.galFrame, text='Number of Galaxies = %d' %self.ngal, font=self.font_settings)
        self.galLabel.pack(side='top',anchor='w')
        self.gal_scale = tk.Scale(self.galFrame, command=self.set_ngal, from_=1, to=50, resolution=1, orient='horizontal', length=0.9*self.wid, width=0.05*self.hgt, showvalue=0)
        self.gal_scale.set(self.ngal)
        self.gal_scale.pack(side='top')
        
        ########### Save images button ########################################
        self.button_save=tk.Button(self.root, text="Save images", font=self.font_settings,  command=self.saveButton)
        
        ############### Layout frames #######################
        self.imgFrame1.grid(row=0, column=0)
        self.imgFrame2.grid(row=0, column=1)
        self.imgFrame3.grid(row=0, column=2)
        self.button_photo.grid(row=1, column=0)
        self.button_cluster.grid(row=1, column=1)
        self.button_lens.grid(row=1, column=2)
        self.redshiftFrame.grid(row=2, column=0, columnspan=3, pady=0.01*self.hgt)
        self.massFrame.grid(row=3, column=0, columnspan=3, pady=0.01*self.hgt)
        self.galFrame.grid(row=4, column=0, columnspan=3, pady=0.01*self.hgt)
        self.button_save.grid(row=5, column=0, columnspan=3, pady=0.01*self.hgt)
        
        self.root.protocol("WM_DELETE_WINDOW", self.handler)
        self.root.mainloop()
    
    def set_mass(self, val):
        self.M=10**(float)(val)
        #self.scaleText.configure(text='10%s' %self.unimap[val])
        self.massLabel.configure(text='Galaxy Mass = %.1e' %self.M)
        
        
    def set_redshift(self, val):
        self.z=(float)(val)
        self.redshiftLabel.configure(text='Redshift = %.1f' %self.z)
        
    def set_ngal(self, val):
        self.ngal=(int)(val)
        self.galLabel.configure(text='Number of Galaxies = %d' %self.ngal)

    def rescale_image(self, im):
        sz=im.size
        #Figure out scaling so that the width/height fits
        rescale=self.imgwid/sz[0]
        im=im.resize((self.imgwid, (int)(rescale*sz[1])), Image.ANTIALIAS)
        return im

    #Quit when you press the x
    def handler(self):
        self.cap.release()
        self.root.quit()

    #Crop the image from the webcam
    def make_square(self, im):
        sz=im.size
        #FInd the smallest side (I'd basically always expect this to be the height)
        if sz[0]>sz[1]: #width>height
            leftedge=(sz[0]-sz[1])//2
            #If it's an odd width, add the remainder to the right edge
            rightedge=sz[0]-((sz[0]-sz[1])//2+(sz[0]-sz[1])%2)
            topedge=0
            bottomedge=sz[1]
        else:
            topedge=(sz[1]-sz[0])//2
            bottomedge=sz[1]-((sz[1]-sz[0])//2+(sz[1]-sz[0])%2)
            leftedge=0
            rightedge=sz[0]
            
        return im.crop((leftedge, topedge, rightedge, bottomedge))

    #Photo button method
    def photoButton(self):
        
        ret,frame=self.cap.read()
        im=cv2.cvtColor(frame,cv2.COLOR_BGR2RGB)
        im=Image.fromarray(im)
        im=self.make_square(im)
        self.img=self.rescale_image(im)
        self.inputTkImage=ImageTk.PhotoImage(self.img)
        self.inputImage.configure(image=self.inputTkImage)
        
        #self.inputImage.image=img
    
    #Generate new cluster based on redshift and mass
    def clusterButton(self):
        k, a1, a2=lenses.gen_random_lenses(self.ngal, pixscale=1.0, npix=488, sig_x=60, posx=244, posy=244, zl=self.z, mass=self.M)
        lm=lens_image.LensMap(k, a1, a2)
        self.lm=lm
        m=pl.log(self.lm.map)
        m=m-m.min()
        print m
        im2=Image.fromarray(pl.uint8(pl.cm.hot(m/m.max())*255))
        self.img2=self.rescale_image(im2)
        self.lensTkImage=ImageTk.PhotoImage(self.img2)
        self.lensImage.configure(image=self.lensTkImage)
        
    #Do the lensing
    def lensButton(self):
        self.img3=lens_image.do_lensing(self.img, self.lm)
        self.img3=self.rescale_image(self.img3)
        self.outputTkImage=ImageTk.PhotoImage(self.img3)
        self.outputImage.configure(image=self.outputTkImage)
       
    def saveButton(self):
        rt='images/'
        os.system('mkdir '+rt) #I have no idea if this will work on mac
        try:
            #This assumes a filename pattern of input_name+number.jpg
            # e.g. "input3.jpg"
            fls=os.listdir(rt)
            input_name='input'
            output_name='output'
            
            #Get all the files with name matching input_name and output_name
            input_fls=[s for s in fls if input_name in s]
            output_fls=[s for s in fls if output_name in s]
            #Find the highest number in these files to save the new files at the next number
            #Should be robust against deleting files 
            
            if len(input_fls)==0 and len(output_fls)==0:
                save_num=0
            else:
                in_nums=pl.array([i.split(input_name)[-1].split('.')[0] for i in input_fls], dtype='int')
                out_nums=pl.array([i.split(output_name)[-1].split('.')[0] for i in output_fls], dtype='int')
                
                save_num=max(in_nums.max(), out_nums.max())+1
            
            self.img.save(rt+input_name+'%d.jpg' %save_num)
            self.img3.save(rt+output_name+'%d.jpg' %save_num)
            
            
            
        except OSError:
            print "path doesn't exist, can't save images"
        
        
        
    

Gui()

############# lower half frame #######################
#frame2=tk.Frame(root)
#frame2.grid(row=1, column=0)
#f2wid=frame2.winfo_width()
#f2hgt=frame2.winfo_height()



########### padding frame #######################
#separator=tk.Frame(frame2, height=f2hgt, width=f2wid/3)
#tk.Label(separator, text='').pack()
#separator.grid(row=0, column=1)




#frame2.columnconfigure(1, pad=f2wid/3)





